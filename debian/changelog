libcatmandu-store-mongodb-perl (0.0806-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Debian Janitor ]
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse
  * relax to (build-)depend unversioned
    on libcatmandu-perl libcpanel-json-xs-perl libmongodb-perl

  [ Jonas Smedegaard ]
  * simplify source helper script copyright-check
  * declare compliance with Debian Policy 4.6.0
  * use debhelper compatibility level 13 (not 12)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Sep 2021 16:16:09 +0200

libcatmandu-store-mongodb-perl (0.0803-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * watch: update usage comment
  * relax to build-depend unversioned on perl:
    needed version of libtest-simple-perl satisfied by perl
    even in oldstable
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Apr 2020 11:58:38 +0200

libcatmandu-store-mongodb-perl (0.0802-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Update watch file: Rewrite usage comment.
  * Simplify rules.
    Stop build-depend on cdbs.
  * Relax to (build-)depend unversioned on libcql-parser-perl
    libmoo-perl libnamespace-clean-perl libtest-exception-perl:
    Needed versions satisfied even in oldstable.
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.3.0.
  * Stop build-depend on dh-buildinfo.
  * Mark build-dependencies needed only for testsuite as such.
  * Wrap and sort control file.
  * Explain Catmandu last in long description.
  * Update copyright info:
    + Extend coverage of packaging.
    + Bump (yes, not extend) coverage for main upstream author.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Feb 2019 00:22:12 +0100

libcatmandu-store-mongodb-perl (0.0700-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Remove deprecated options.
    + Replace deprecated driver method calls.
      Closes: Bug#877535. Thanks to Adrian Bunk.
    + Use the new Catmandu::CQLSearchable role.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Use substitution strings.
    + Mangle version strings to consistently expand to 4 digits.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on licensecheck.
  * Update package relations:
    + Tighten (build-)dependency on libmongodb-perl.
    + Tighten to (build-)depend on recent libcatmandu-perl.
    + (Build-)depend on recent libcpanel-json-xs-perl (not
      libjson-maybexs-perl).
  * Modernize Vcs-* fields:
    + Consistently use git (not cgit) in path.
    + Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.1.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Fix quote verbatim Licnse-Grant for papckaging.
    + Bump (yes not extend) coverage for main upstream author.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Oct 2017 20:15:56 +0200

libcatmandu-store-mongodb-perl (0.0501-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
  * Drop CDBS get-orig-source target: Use gbp import-orig --uscan.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Modernize Vcs-Git field: Use https protocol.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop obsolete lintian override regarding debhelper 9.
  * Update copyright info: Extend coverage of Debian packaging.
  * Update package relations:
    + (Build-)depend on libcql-parser-perl.
    + Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Jan 2017 10:56:05 +0100

libcatmandu-store-mongodb-perl (0.0403-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Add drop method.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.7.
  * Update copyright info:
    + Extend copyright of packaging to cover current year.
    + Bump (yes, not extend) copyright for main upstream author to cover
      current year.
  * Update package relations:
    + (Build-)depend on recent libnamespace-clean-perl.
    + Favor recent perl over recent libtest-simple-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 09 Feb 2016 07:31:23 +0100

libcatmandu-store-mongodb-perl (0.0402-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Sort and newline-delimit dependencies.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend copyright of packaging to cover current year.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Update package relations:
    + (Build-)depend on recent libjson-maybexs-perl (not libjson-perl).
    + Tighten (build-)dependency on libmongodb-perl.
    + Build-depend on libtest-warn-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 27 Oct 2015 23:29:46 +0100

libcatmandu-store-mongodb-perl (0.0303-3) unstable; urgency=medium

  * Team upload
  * Mark package as autopkgtestable
  * Remove 'perl' alternative from the libmodule-build-perl build-dependency

 -- Damyan Ivanov <dmn@debian.org>  Wed, 10 Jun 2015 13:32:49 +0000

libcatmandu-store-mongodb-perl (0.0303-2) unstable; urgency=medium

  * Team upload.
  * Add missing (build) dependency on libjson-perl.
    Detected by the reproducible-build team.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 May 2015 00:38:08 +0200

libcatmandu-store-mongodb-perl (0.0303-1) unstable; urgency=low

  * Initial Release.
    Closes: bug#766243.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 21 Oct 2014 22:05:52 +0200
